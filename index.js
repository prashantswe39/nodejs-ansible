var http = require('http');
var server = http.createServer(function (request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.end("Hello from index.js World\n");
});
server.listen(8080);
console.log("Server running at http://0.0.0.0:8080/");
